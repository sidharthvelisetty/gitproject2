import pytest
from src.testoperation import addme, multiplyme 

@pytest.mark.test1
def test_addme():
    assert addme(10) == 12
    assert addme(10,20) == 30
    assert addme('hello ', 'world') == 'hello world'

@pytest.mark.test2
def test_multiplyme():
    assert multiplyme(5,6) == 30
    assert multiplyme(10) == 30
    assert multiplyme('Hello') == 'HelloHelloHello'